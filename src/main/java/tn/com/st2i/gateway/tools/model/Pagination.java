package tn.com.st2i.gateway.tools.model;

import lombok.Data;

@Data
public class Pagination {

	private Integer offSet;
	private Integer limit;

}
