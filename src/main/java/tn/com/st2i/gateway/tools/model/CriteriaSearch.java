package tn.com.st2i.gateway.tools.model;

import lombok.Data;

@Data
public class CriteriaSearch {

	private String key;
	private String value;
	private String specificSearch;
	
}
