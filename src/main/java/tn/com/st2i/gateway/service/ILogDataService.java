package tn.com.st2i.gateway.service;

import tn.com.st2i.gateway.tools.model.LogEvent;
import tn.com.st2i.gateway.tools.model.SendObject;

public interface ILogDataService {
	
	public SendObject saveLogDataFromMicroService(LogEvent logEvent);

}
