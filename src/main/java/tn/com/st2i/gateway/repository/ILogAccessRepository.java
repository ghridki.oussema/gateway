package tn.com.st2i.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.com.st2i.gateway.model.LogAccess;

@Repository
public interface ILogAccessRepository extends JpaRepository<LogAccess, Long> {

}
