package tn.com.st2i.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.com.st2i.gateway.model.LogData;

public interface ILogDataRepository extends JpaRepository<LogData, Long> {

}
